import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {
  MzNavbarModule,
  MzParallaxModule,
  MzButtonModule,
  MzCardModule,
  MzIconMdiModule,
  MzValidationModule,
  MzInputModule,
  MzTextareaModule,
  MzModalModule,
  MzSpinnerModule,
  MzDropdownModule,
  MzBadgeModule,
  MzSelectModule,
  MzRadioButtonModule,
  MzCheckboxModule,
  MzDatepickerModule, MzTooltipModule, MzCollapsibleModule,
} from 'ngx-materialize';
import {ScrollToModule} from '@nicky-lenaers/ngx-scroll-to';
import {AppRoutingModule} from './app-routing.module';
import {HomeComponent} from './component/home/home.component';
import {SignUpComponent} from './component/user/signup/signup.component';
import {SignInComponent} from './component/user/signin/signin.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {ShopComponent} from './component/product/shop/shop.component';
import {CartComponent} from './component/product/cart/cart.component';
import {CheckOutComponent} from './component/product/checkout/checkout.component';
import {JwtInterceptor} from "./_helper/jwt-interceptor";
import { ManageComponent } from './component/manage/manage.component';
import {AdminInventoryComponent} from "./component/manage/inventory/inventory.component";
import { AdminOrderComponent } from './component/manage/order/admin-order.component';
import { OrdersComponent } from './component/user/orders/orders.component';
import {NgxStarsModule} from "ngx-stars";
import { MyProfileComponent } from './component/user/my-profile/my-profile.component';
import {AdminFeedbackComponent} from "./component/manage/feedback/admin-feedback.component";
import {FeedbackComponent} from "./component/product/feedback/feedback.component";
import { AdminDashboardComponent } from './component/manage/dashboard/admin-dashboard.component';
import {AdminSupplierComponent} from "./component/manage/supplier/admin-supplier.component";
import {DataTablesModule} from "angular-datatables";
import { LoadWalletComponent } from './component/product/load-wallet/load-wallet.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SignUpComponent,
    SignInComponent,
    ShopComponent,
    CartComponent,
    CheckOutComponent,
    ManageComponent,
    AdminInventoryComponent,
    AdminOrderComponent,
    OrdersComponent,
    MyProfileComponent,
    AdminFeedbackComponent,
    FeedbackComponent,
    AdminDashboardComponent,
    AdminSupplierComponent,
    LoadWalletComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ScrollToModule.forRoot(),
    MzNavbarModule,
    MzParallaxModule,
    MzButtonModule,
    MzCardModule,
    MzIconMdiModule,
    MzValidationModule,
    AppRoutingModule,
    ReactiveFormsModule,
    MzInputModule,
    MzTextareaModule,
    MzModalModule,
    MzDatepickerModule,
    MzBadgeModule,
    MzSpinnerModule,
    MzTooltipModule,
    MzRadioButtonModule,
    MzDropdownModule,
    MzSelectModule,
    MzCheckboxModule,
    HttpClientModule,
    FormsModule,
    NgxStarsModule,
    DataTablesModule,
    MzCollapsibleModule
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true},
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
