import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './component/home/home.component';
import {SignUpComponent} from "./component/user/signup/signup.component";
import {ShopComponent} from "./component/product/shop/shop.component";
import {CheckOutComponent} from "./component/product/checkout/checkout.component";
import {ManageComponent} from "./component/manage/manage.component";
import {AuthGuard} from "./_guard/auth-guard";
import {OrdersComponent} from "./component/user/orders/orders.component";
import {MyProfileComponent} from "./component/user/my-profile/my-profile.component";
import {AdminInventoryComponent} from "./component/manage/inventory/inventory.component";
import {AdminDashboardComponent} from "./component/manage/dashboard/admin-dashboard.component";
import {AdminSupplierComponent} from "./component/manage/supplier/admin-supplier.component";


const routes: Routes = [
  {path: '', pathMatch: 'full', component: HomeComponent},
  {path: 'signup', component: SignUpComponent},
  {path: 'shop', component: ShopComponent},
  {path: 'checkout', component: CheckOutComponent, canActivate: [AuthGuard]},
  {path: 'view-orders', component: OrdersComponent, canActivate: [AuthGuard]},
  {path: 'my-profile', component: MyProfileComponent, canActivate: [AuthGuard]},
  {
    path: 'admin/manage', component: ManageComponent, canActivate: [AuthGuard], children: [
      {path: '', component: AdminDashboardComponent},
      {path: 'inventory', component: AdminInventoryComponent},
      {path: 'supplier', component: AdminSupplierComponent}]
  },
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})


export class AppRoutingModule {
}
