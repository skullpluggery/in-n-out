import {Order} from "./order";

export class Invoice {
  order: Order;
  total: number;
}

