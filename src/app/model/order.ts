import {User} from "./user";
import {OrderItemList} from "./order-item-list";


export class Order {
  id : number;
  user: User;
  remarks: string;
  orderItemList: OrderItemList[];
  status : string;
  useLoyalty: boolean;
  useWallet: boolean;
}
