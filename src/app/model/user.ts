import {LoyaltyCard} from "./loyalty-card";

export class User {
  id:number;
  name: String;
  username: String;
  email: String;
  contactNumber:String;
  address: String;
  type: number;
  password: String;
  birthdate: String;
  gender: String;
  loyaltyCard: LoyaltyCard;
  classification: String;
  activeLastMonth: boolean;
}
