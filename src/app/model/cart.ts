import {CartItem} from "./cart-item";

export class Cart {
  cartItems: CartItem[];
  itemCount: number;
  totalQTY: number;
  totalPrice: number;
}
