export class Supplier {
  id: number;
  supplierName: string;
  address: string;
  contactNumber: string;
}
