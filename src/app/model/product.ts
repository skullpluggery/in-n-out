export class Product {
  id: number
  name: string;
  description: string;
  categories: string;
  price: number;
  img_url: string;
  createdAt: string;
  updatedAt: string;
}
