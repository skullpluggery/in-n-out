import {Supplier} from "./supplier";
import {Product} from "./product";

export class Inventory {
  id: number;
  supplier: Supplier;
  product: Product;
  quantity: number;
  createdAt: string;
}


