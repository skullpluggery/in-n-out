import {Product} from "./product";

export class CartItem {
  product: Product;
  subtotalPrice: number;
  quantity: number;

}
