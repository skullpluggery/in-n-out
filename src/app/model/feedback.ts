import {User} from "./user";

export class Feedback {
  user: User;
  subject: string;
  message: string;
  stars: number;
  date: string;
}
