import {Product} from "./product";

export class OrderItemList {
    product: Product
    quantity: number;
}
