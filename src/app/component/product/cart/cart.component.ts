import {AfterViewInit, Component, OnInit} from '@angular/core';
import {AppComponent} from "../../../app.component";
import swal from "sweetalert";


@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css'],
})
export class CartComponent implements OnInit {

  cart = AppComponent.cart;

  constructor() {

  }

  ngOnInit() {

  }

  decrementQTY(product) {
    AppComponent.cart.cartItems.forEach(item => {
      if (item.product.id === product.product.id) {
        if (item.quantity != 1) {
          item.quantity--;
          item.subtotalPrice = item.product.price * item.quantity;
          AppComponent.syncCart();
        }
      }
    });
  }

  incrementQTY(product) {
    AppComponent.cart.cartItems.forEach(item => {
      if (item.product.id === product.product.id) {
        item.quantity++;
        item.subtotalPrice = item.product.price * item.quantity;
        AppComponent.syncCart();
      }
    });
  }

  removeProduct(cartItem) {
    //@ts-ignore
    swal({
      title: "Remove " + cartItem.product.name + " from cart?",
      icon: "warning",
      buttons: true,
    }).then((remove) => {
      if(remove){
        let ctr = 0;
        AppComponent.cart.cartItems.forEach(item => {
          if (item.product.id === cartItem.product.id) {
            AppComponent.cart.cartItems.splice(ctr,1);
            AppComponent.syncCart();
            return;
          }
          ctr++;
        });
      }
    });
  }
}
