import { Component, OnInit } from '@angular/core';
import {FeedbackService} from "../../../service/feedback/feedback.service";
import {Feedback} from "../../../model/feedback";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import swal from "sweetalert";
import * as moment from 'moment';

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.css']
})
export class FeedbackComponent implements OnInit {

  user = JSON.parse(localStorage['currentUser'] || null);
  feedbackForm: FormGroup;

  feedback: Feedback = {
    user: this.user,
    stars: 0,
    subject: '',
    message: '',
    date: ''
  }

  errorMessages = {
    subject: {
      required: 'Subject is required.',
      minlength: 'Subject must be at least 5 characters long.',
      maxlength: 'Subject cannot be more than 20 characters long.',
    },
    messageContent: {
      required: 'Message is required.',
      minlength: 'Message must be at least 10 characters long.',
      maxlength: 'Message cannot be more than 255 characters long.',
    },
  }

  constructor(private feedbackService:FeedbackService,
              private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.buildForm();
  }

  buildForm() {
    this.feedbackForm = this.formBuilder.group({
      subject: [this.feedback.subject, Validators.compose([
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(50),
      ]),
      ],
      messageContent: [this.feedback.message, Validators.compose([
        Validators.required,
        Validators.minLength(10),
        Validators.maxLength(255),
      ]),
      ],
    })
  }

  onSubmit(){
    this.feedback.subject = this.feedbackForm.controls.subject.value.toString().trim();
    this.feedback.message = this.feedbackForm.controls.messageContent.value.toString().trim();
    this.feedback.date = moment().format();

    this.feedbackService.store(this.feedback).subscribe(result=>{
      if(result){
        //@ts-ignore
        swal({
          title: "Thank you for your feedback!",
          text: "You will be redirected in a few seconds...",
          icon: "success",
          buttons: false,
          timer: 3000,
        }).then(()=>{
          this.resetForm();
        });
      }
    })

  }

  resetForm() {
    this.feedbackForm.reset();
    location.reload();
  }

  skip(){
    //@ts-ignore
    swal({
      title: "It's okay! We still thank you for ordering!",
      text: "You will be redirected in a few seconds...",
      icon: "success",
      buttons: false,
      timer: 3000,
    }).then(()=>{
      this.resetForm();
    });
  }

  onRatingSet(rating){
    this.feedback.stars = rating;
  }

}
