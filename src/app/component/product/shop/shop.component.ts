import {AfterViewInit, Component, OnInit} from '@angular/core';
import {Title} from "@angular/platform-browser";
import {fadeInAnimation} from "../../../_animation";
import {InventoryService} from "../../../service/inventory/inventory.service";
import {AppComponent} from "../../../app.component";
import {CartItem} from "../../../model/cart-item";
import swal from "sweetalert";
import shave from 'shave';
import {Inventory} from "../../../model/inventory";
import {User} from "../../../model/user";
import {UserService} from "../../../service/user/user.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {LoyaltyCard} from "../../../model/loyalty-card";

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.css'],
  animations: [fadeInAnimation],
  host: {'[@fadeInAnimation]': ''}
})
export class ShopComponent implements OnInit, AfterViewInit {

  keyword;
  user: User  = JSON.parse(localStorage['currentUser'] || null);
  inventories: Inventory[];
  categories = ["Specialty", "Party", "Dessert", "Salad"];

  constructor(private title: Title,
              private inventoryService: InventoryService,
              private formBuilder: FormBuilder,
              private userService:UserService) {

    title.setTitle("Shop - In N Out");
  }

  sendEmailForm: FormGroup;
  searchForm: FormGroup;

  buildForm(){
    this.searchForm = this.formBuilder.group({
      search: ['', Validators.compose([
            Validators.pattern(
        '([a-zA-z])*'),
    ]),
    ],
    }),

    this.sendEmailForm = this.formBuilder.group({
      emailAdd: ['', Validators.compose([
        Validators.required,
        Validators.pattern(
          '^([a-zA-Z0-9]+(?:[.-]?[a-zA-Z0-9]+)*@[a-zA-Z0-9]+(?:[.-]?[a-zA-Z0-9]+)*\\.[a-zA-Z]{2,7})$'),
      ]),
      ],
    });
  }

  ngOnInit() {
    this.getAllProducts();
    $('#loadModal').modal();
    this.buildForm();
    $('#sendEmailModal').modal();

      if(this.user!=null && this.user.type!=0 && this.user.activeLastMonth){
        if(this.user.classification==="At risk") {
          swal("You must be really hungry. Don't worry, we got you ;)",
            "Redeem points through code: INNOUT100 and start ordering now!");
        }
        else if(this.user.classification==="Normal"){
          $('#sendEmailModal').modal('open');
        }
        else{
          swal("Thank you for your loyalty.",
            "As a reward, redeem points through code: INNOUT50!");
        }
      }
  }

  ngAfterViewInit() {
    this.userService.syncUser().subscribe(result=>{
      localStorage.setItem('currentUser', JSON.stringify(result));
      this.user = result;
    });
  }

  getAllProducts() {
    this.inventoryService.getAllInventory().subscribe(result => {
      this.inventories = result;
    });
    setTimeout(() =>
      shave('.food-desc', 100), 500);

  }



  currentCategory = '';

  errorMessage = {
    search: {
      pattern: 'No special characters allowed!'
    }
  };

  setCategory(category) {
    $('#category-' + this.currentCategory).removeClass('crimson');
    this.currentCategory = category;
    $('#category-' + this.currentCategory).addClass('crimson');

    this.search(category);
  }

  reset() {
    this.getAllProducts();
    $('#category-' + this.currentCategory).removeClass('crimson');
    this.keyword = '';
  }

  addToCart(product) {
    //@ts-ignore
    swal({
      title: "Add " + product.name + " to cart?",
      text: "You can specify the quantity later on.",
      icon: "warning",
      buttons: true,
    }).then((add) => {
      if (add) {
        let cartItem: CartItem = {
          product: product,
          quantity: 1,
          subtotalPrice: 0
        }


        let updateItem = false;
        AppComponent.cart.cartItems.forEach(item => {
          if (item.product.id === cartItem.product.id) {
            item.quantity++;
            item.subtotalPrice = item.product.price * item.quantity;
            updateItem = true;
          }
        })

        if (!updateItem) {
          cartItem.subtotalPrice = cartItem.product.price * cartItem.quantity;
          AppComponent.cart.cartItems.push(cartItem);
          AppComponent.cart.totalPrice += cartItem.product.price * cartItem.quantity;
          AppComponent.cart.totalQTY += cartItem.quantity;
          AppComponent.syncCart();
        }
      }
    });
  }

  searchProductOnKeyPress() {
    if (this.keyword == '') {
      this.getAllProducts();
      return;
    }
    this.search(this.keyword);
  }

  search(keyword) {
    this.inventoryService.showInventoryByNameOrCategory(keyword).subscribe(result => {
      this.inventories = result;
    });
    setTimeout(() =>
      shave('.food-desc', 100), 300);
  }

  loyaltyCard = new LoyaltyCard();

  sendEmail(){

    this.user.loyaltyCard.loyaltyPoints += 15;
    console.log(this.user);
    this.userService.updateLoyaltyCard(this.user.loyaltyCard).subscribe(()=>{
      //@ts-ignore
      swal({
        title: "",
        text: "You received 15 points for inviting a friend!",
        icon: "success",
        buttons: false,
        timer: 2000,
      });
      $('#sendEmailModal').modal('close');
    },(error: any) => {
      console.log(error);
    });
  }
}
