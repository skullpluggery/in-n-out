import {Component, OnInit} from '@angular/core';
import swal from "sweetalert";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {UserService} from "../../../service/user/user.service";
import {User} from "../../../model/user";
import {load} from "@angular/core/src/render3/instructions";

@Component({
  selector: 'app-load-wallet',
  templateUrl: './load-wallet.component.html',
  styleUrls: ['./load-wallet.component.css']
})
export class LoadWalletComponent implements OnInit {

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService) {
  }

  user: User = JSON.parse(localStorage['currentUser'] || null);
  loadForm: FormGroup;

  errorMessageResources = {
    loadValue: {
      required: "Code is required",
    }
  }

  ngOnInit() {
    this.buildForm();
  }

  buildForm() {
    this.loadForm = this.formBuilder.group({
      loadValue: ['', Validators.compose([
        Validators.required,
      ]),
      ],
    })
  }

  resetForm() {
    this.loadForm.reset();
  }

  onSubmit() {
    if (!this.loadForm.valid) {
      return;
    }

    let loadWallet = true;
    switch (this.loadForm.controls.loadValue.value.toString()) {
      case 'INNOUT50':
        this.user.loyaltyCard.loadPoints += 50;
        break;
      case 'INNOUT100':
        this.user.loyaltyCard.loadPoints += 100;
        break;
      case 'INNOUT150':
        this.user.loyaltyCard.loadPoints += 150;
        break;
      case 'INNOUT200':
        this.user.loyaltyCard.loadPoints += 200;
        break;
      default:
        loadWallet = false;
    }

    if (loadWallet) {
      //@ts-ignore
      swal({
        title: "Proceed now?",
        text: "Your load will be added to your wallet after this.",
        icon: "warning",
        buttons: ["Cancel", "Proceed"],
      }).then(proceed => {
        if (proceed) {
          this.userService.updateLoyaltyCard(this.user.loyaltyCard).subscribe(() => {
            //@ts-ignore
            swal({
              title: "Success!",
              text: "Your load was sent in your wallet!",
              icon: "success",
            }).then(() => {
              location.reload();
            });
          });
        }
      });
    } else {
      //@ts-ignore
      swal({
        title: "Code does not exist!",
        text: "Please double check your code.",
        icon: "error",
      })
    }
}

}
