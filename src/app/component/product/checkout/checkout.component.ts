import {AfterViewInit, Component, OnInit} from '@angular/core';
import {fadeInAnimation} from "../../../_animation";
import {Title} from "@angular/platform-browser";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AppComponent} from "../../../app.component";
import {Order} from "../../../model/order";
import {OrderItemList} from "../../../model/order-item-list";
import {OrderService} from "../../../service/order/order.service";
import {Router} from "@angular/router";
import shave from 'shave';
import swal from "sweetalert";
import {User} from "../../../model/user";
import {UserService} from "../../../service/user/user.service";

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css'],
  animations: [fadeInAnimation],
  host: {'[@fadeInAnimation]': ''}
})
export class CheckOutComponent implements OnInit, AfterViewInit {

  cartForm: FormGroup;
  cart = AppComponent.cart;
  user: User = JSON.parse(localStorage['currentUser'] || null);

  order: Order = {
    id: 0,
    user: this.user,
    remarks: '',
    orderItemList: [],
    status: "PENDING",
    useLoyalty: false,
    useWallet: false
  };

  errorMessages = {
    email: {
      pattern: 'Invalid email format. Ex: example@example.com',
      required: 'E-mail is required.',
    },
    contactNumber: {
      pattern: 'Invalid cellphone number format. Ex: XXXX-XXX-XXXX',
      required: 'Contact Number is required.',
    },
    address: {
      minlength: 'Address must be at least 10 characters long.',
      maxlength: 'Address cannot be more than 255 characters long.',
      required: 'Address is required.',
    },
    remarks: {
      maxlength: 'Remarks cannot be more than 800 characters long.'
    },
  }

  constructor(private title: Title,
              private formBuilder: FormBuilder,
              private orderService: OrderService,
              private userService: UserService,
              private router: Router,) {
    title.setTitle('Check Out - In N Out');
  }

  ngOnInit() {
    this.buildForm(); //Produces error when not called
    if (this.cart.cartItems.length == 0) {
      this.router.navigate(['/shop']);
    }
    $('#feedbackModal').modal();
  }

  ngAfterViewInit(): void {
    this.userService.syncUser().subscribe(result => {
      localStorage.setItem('currentUser', JSON.stringify(result));
      this.user = result;
    });
    setTimeout(() =>
      shave('.checkout-product-desc', 200), 500);
  }

  decrementQTY(product) {
    this.cartForm.controls.useLoyalty.setValue(false);
    AppComponent.cart.cartItems.forEach(item => {
      if (item.product.id === product.product.id) {
        if (item.quantity != 1) {
          item.quantity--;
          item.subtotalPrice = item.product.price * item.quantity;
          AppComponent.syncCart();
        }
      }
    });
  }

  incrementQTY(product) {
    this.cartForm.controls.useLoyalty.setValue(false);
    AppComponent.cart.cartItems.forEach(item => {
      if (item.product.id === product.product.id) {
        item.quantity++;
        item.subtotalPrice = item.product.price * item.quantity;
        AppComponent.syncCart();
      }
    });
    if (this.user.loyaltyCard.loadPoints < this.cart.totalPrice) {
      this.cartForm.controls.paymentMethod.setValue("cod");
    }
  }

  removeProduct(cartItem) {
    this.cartForm.controls.useLoyalty.setValue(false);
    //@ts-ignore
    swal({
      title: "Remove " + cartItem.product.name + " from cart?",
      icon: "warning",
      buttons: true,
    }).then((remove) => {
      if (remove) {
        let ctr = 0;
        AppComponent.cart.cartItems.forEach(item => {
          if (item.product.id === cartItem.product.id) {
            AppComponent.cart.cartItems.splice(ctr, 1);
            AppComponent.syncCart();
            if (AppComponent.cart.cartItems.length == 0) {
              this.router.navigate(['/shop']);
            }
            return;
          }
          ctr++;
        });
      }
    });

  }


  buildForm() {
    this.cartForm = this.formBuilder.group({
      email: [this.order.user.email, Validators.compose([
        Validators.required,
        Validators.pattern(
          '^([a-zA-Z0-9]+(?:[.-]?[a-zA-Z0-9]+)*@[a-zA-Z0-9]+(?:[.-]?[a-zA-Z0-9]+)*\\.[a-zA-Z]{2,7})$'),
      ]),
      ],
      contactNumber: [this.order.user.contactNumber, Validators.compose([
        Validators.required,
        Validators.pattern(
          '^([0-9]( |-)?)?(\\(?[0-9]{4}\\)?|[0-9]{3})( |-)?([0-9]{3}( |-)?[0-9]{4}|[a-zA-Z0-9]{7})'),
      ]),
      ],
      address: [this.order.user.address, Validators.compose([
        Validators.required,
        Validators.minLength(10),
        Validators.maxLength(255),
      ]),
      ],
      remarks: [this.order.remarks, Validators.compose([
        Validators.maxLength(800),
      ]),
      ],
      useLoyalty: [this.order.useLoyalty],
      paymentMethod: ['cod'],
    });

  }

  useLoyaltyTicked() {
    if (!this.cartForm.controls.useLoyalty.value) {
      if (this.user.loyaltyCard.loyaltyPoints > this.cart.totalPrice) {
        this.cart.totalPrice = 0;
      } else {
        this.cart.totalPrice -= this.user.loyaltyCard.loyaltyPoints;
      }
    } else {
      AppComponent.syncCart();
    }
  }

  onSubmit() {

    this.cart.cartItems.forEach(cartItem => {
      let orderItem: OrderItemList = {
        product: cartItem.product,
        quantity: cartItem.quantity
      };
      this.order.orderItemList.push(orderItem)
    });


    this.order.useLoyalty = this.cartForm.controls.useLoyalty.value;
    this.order.remarks = this.cartForm.controls.remarks.value.trim();

    if (this.cartForm.controls.paymentMethod.value.toString() == 'wallet') {
      this.order.useWallet = true;
    }

    console.log(this.order);
    this.orderService.store(this.order).subscribe(() => {
      this.cartForm.reset();
      //@ts-ignore
      swal({
        title: "We've received your order!",
        text: "Would you like to give us a feedback?",
        icon: "success",
        buttons: true,
      }).then((yes) => {
        if (yes) {
          $('#feedbackModal').modal('open');
        } else {
          location.reload();
        }
      });
    })

  }
}
