import {Component, OnDestroy, OnInit} from '@angular/core';
import {Feedback} from "../../../model/feedback";
import {FeedbackService} from "../../../service/feedback/feedback.service";
import {Title} from "@angular/platform-browser";
import * as moment from 'moment';
import {Inventory} from "../../../model/inventory";
import {Subject} from "rxjs/internal/Subject";


@Component({
  selector: 'app-admin-feedback',
  templateUrl: './admin-feedback.component.html',
  styleUrls: ['./admin-feedback.component.css']
})
export class AdminFeedbackComponent implements OnInit, OnDestroy {

  feedbackList : Feedback[];
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<Inventory> = new Subject();
  constructor(private title: Title, private feedbackService: FeedbackService) {
  }

  ngOnInit(){
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 5
    };
    this.getAllFeedback();
  }

  getAllFeedback() {
    this.feedbackService.getAll().subscribe(result => {
      this.feedbackList = result;
      this.feedbackList = result;
      this.feedbackList.forEach(feedback=>{
        feedback.date = moment(feedback.date).format("MMM DD YYYY h:mm:ss A");
      });
      this.dtTrigger.next();
    });
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }
}

