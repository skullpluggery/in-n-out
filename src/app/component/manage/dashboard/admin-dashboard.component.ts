import { Component, OnInit } from '@angular/core';
import {Title} from "@angular/platform-browser";

@Component({
  selector: 'app-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.css']
})
export class AdminDashboardComponent implements OnInit {

  constructor(private title: Title) {
    title.setTitle('Dashboard - In N Out');
  }

  ngOnInit() {
    $('#admin-nav li').removeClass('active');
    $('#admin-nav li#dashboard').addClass('active');
  }

}
