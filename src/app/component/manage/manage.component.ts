import { Component, OnInit } from '@angular/core';
import {User} from "../../model/user";
import {Router} from "@angular/router";
import {Title} from "@angular/platform-browser";
import {UserService} from "../../service/user/user.service";

@Component({
  selector: 'app-manage',
  templateUrl: './manage.component.html',
  styleUrls: ['./manage.component.css']
})
export class ManageComponent implements OnInit {

  constructor(private router: Router,
              private title: Title,
              private userService:UserService){
    title.setTitle("Admin - In N Out")
  }
  user: User  = JSON.parse(localStorage['currentUser'] || null);
  ngOnInit(){
    if(!this.user||this.user.type!=0){
      this.router.navigate(['/']);
    }else{
      this.router.navigate(['admin/manage']);
    }
    $("#homeNav").hide();
  }
  signOut() {
    this.userService.signOut()
  }
}

