import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Inventory} from "../../../model/inventory";
import {Subject} from "rxjs/internal/Subject";
import {InventoryService} from "../../../service/inventory/inventory.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Title} from "@angular/platform-browser";
import {DataTableDirective} from "angular-datatables";
import swal from "sweetalert";
import {ProductService} from "../../../service/product/product.service";
import {Supplier} from "../../../model/supplier";
import {Product} from "../../../model/product";
import {SupplierService} from "../../../service/supplier/supplier.service";
import * as moment from 'moment';

@Component({
  selector: 'app-inventory',
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.css']
})
export class AdminInventoryComponent implements OnInit, OnDestroy {

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  inventoryForm: FormGroup;
  addInventoryForm: FormGroup;
  suppliers: Supplier[] = [];
  inventories: Inventory[] = [];
  inventory : Inventory = {
    id: 0,
    supplier: null,
    product: null,
    quantity: 0,
    createdAt: ''
  };

  supplier : Supplier={
    id: 0,
    supplierName: '',
    address: '',
    contactNumber: ''
  };

  product : Product={
    id: 0,
    name: '',
    description: '',
    categories: '',
    price: 0,
    img_url: '',
    createdAt:'',
    updatedAt:''
  };

  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<Inventory> = new Subject();

  constructor(private title: Title,
              private inventoryService: InventoryService,
              private productService: ProductService,
              private supplierService: SupplierService,
              private formBuilder: FormBuilder) {

    title.setTitle('Inventory - In N Out');
  }

  ngOnInit() {
    $('#admin-nav li').removeClass('active');
    $('#admin-nav li#inventory').addClass('active');
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 5
    };
    this.getAllInventories();
    this.getAllSuppliers();
    this.buildForm();
    $('#inventoryModal').modal();
    $('#addInventoryModal').modal();
  }

  errorMessageResources = {
    name: {
      required: 'Username or E-mail is required.',
    },
    description: {
      required: 'Password is required.',
    }
  }

  buildForm() {
    this.inventoryForm = this.formBuilder.group({
      name: ['', Validators.compose([
        Validators.required,
      ]),
      ],
      description: ['', Validators.compose([
        Validators.required,
      ]),
      ],
      quantity: ['', Validators.compose([
        Validators.required,
      ]),
      ],
      category: ['', Validators.compose([
        Validators.required,
      ]),
      ],
      price: ['', Validators.compose([
        Validators.required,
      ]),
      ],
      supplierDropdown: ['', Validators.compose([
        Validators.required,
      ]),
      ],
    }),
    this.addInventoryForm = this.formBuilder.group({
      addImage: ['', Validators.compose([
        Validators.required,
      ]),
      ],
      addname: ['', Validators.compose([
        Validators.required,
      ]),
      ],
      adddescription: ['', Validators.compose([
        Validators.required,
      ]),
      ],
      addquantity: ['', Validators.compose([
        Validators.required,
      ]),
      ],
      addcategory: ['', Validators.compose([
        Validators.required,
      ]),
      ],
      addprice: ['', Validators.compose([
        Validators.required,
      ]),
      ],
      addSupplierDropdown: ['', Validators.compose([
        Validators.required,
      ]),
      ],
    })
  }

  edit(inventory: Inventory){
    this.inventory = inventory;
    this.inventory.id = inventory.id;

    this.inventory.createdAt = moment().format(inventory.createdAt);
    this.inventoryForm.controls.supplierDropdown.setValue(inventory.supplier.id);
    this.inventoryForm.controls.name.setValue(inventory.product.name);
    this.inventoryForm.controls.description.setValue(inventory.product.description);
    this.inventoryForm.controls.quantity.setValue(inventory.quantity);
    this.inventoryForm.controls.category.setValue(inventory.product.categories);
    this.inventoryForm.controls.price.setValue(inventory.product.price);
    $('#inventoryModal').modal('open');
  }

  openAddModal(){
    $('#addInventoryModal').modal('open');
  }

  image = "assets/product/default.jpg";
  img_url = '';
  upload(event: any) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.readAsDataURL(event.target.files[0]);

      reader.onload = (event: ProgressEvent) => {
        this.image = (<FileReader>event.target).result;
      }

      this.productService.uploadImage(event.target.files[0]).subscribe(result => {
        this.img_url = event.target.files[0].name;
      });
    }
  }
  addInventory(){

    this.product.name = this.addInventoryForm.controls.addname.value;
    this.product.img_url = this.img_url;
    this.product.description = this.addInventoryForm.controls.adddescription.value;
    this.product.categories = this.addInventoryForm.controls.addcategory.value;
    this.product.price = this.addInventoryForm.controls.addprice.value;

    this.inventory.product = this.product;
    this.inventory.supplier = this.addInventoryForm.controls.addSupplierDropdown.value;
    this.inventory.quantity = this.addInventoryForm.controls.addquantity.value;

    this.inventoryService.store(this.inventory).subscribe((response) => {
      //@ts-ignore
      swal({
        title: "Success!",
        text: "Added new item",
        icon: "success",
        buttons: false,
        timer: 2000,
      }).then(() => {
        this.rerender();
        $('#addInventoryModal').modal('close');
      });
    }, (error: any) => {
      console.log(error);
    });
  }

  editInventory(){

    this.inventory.supplier.id = this.inventoryForm.controls.supplierDropdown.value;
    this.inventory.product.name = this.inventoryForm.controls.name.value;
    this.inventory.product.description = this.inventoryForm.controls.description.value;
    this.inventory.quantity = this.inventoryForm.controls.quantity.value;
    this.inventory.product.categories = this.inventoryForm.controls.category.value;
    this.inventory.product.price = this.inventoryForm.controls.price.value;


    console.log(this.inventory);
    this.inventoryService.edit(this.inventory).subscribe(() => {
      //@ts-ignore
      swal({
        title: "Update Success!",
        text: "Inventory information has been updated.",
        icon: "success",
        buttons: false,
        timer: 2000,
      }).then(() => {
        this.rerender();
        $('#inventoryModal').modal('close');
      });
    }, (error: any) => {
      console.log(error);
    });

    this.productService.edit(this.inventory.product).subscribe(() => {
    }, (error: any) => {
      console.log(error);
    });
  }

  delete(inventory){
//@ts-ignore
    swal({
      title: "Delete Product?",
      text: "Are you sure you want to delete " + inventory.product.name + "?",
      icon: "warning",
      buttons: true,
    }).then(yes => {
      if (yes) {
        this.inventoryService.delete(inventory.id).subscribe(() => {
          this.rerender();
          //@ts-ignore
          swal({
            title: "Delete Success!",
            text: "Product information has been deleted.",
            icon: "success",
            buttons: false,
            timer: 2000,
          });
        }, (error: any) => {
          console.log(error);
        });
        this.productService.delete(inventory.product.id).subscribe(() => {
        }, (error: any) => {
          console.log(error);
        });

      }
    });
  }

  resetForm() {
    this.inventoryForm.reset();
  }

  getAllInventories() {
    this.inventoryService.getAllInventory().subscribe(result => {
      this.inventories = result;
      this.dtTrigger.next();
    })
  }

  getAllSuppliers() {
    this.supplierService.getAllSuppliers().subscribe(result => {
      this.suppliers = result;
    });
  }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      this.getAllInventories();
    });
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }

}
