import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Supplier} from "../../../model/supplier";
import {SupplierService} from "../../../service/supplier/supplier.service";
import {Title} from "@angular/platform-browser";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import swal from "sweetalert";
import {Subject} from "rxjs/internal/Subject";
import {DataTableDirective} from "angular-datatables";

@Component({
  selector: 'app-admin-supplier',
  templateUrl: './admin-supplier.component.html',
  styleUrls: ['./admin-supplier.component.css']
})
export class AdminSupplierComponent implements OnInit, OnDestroy {

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  
  editSupplierForm: FormGroup;
  addSupplierForm: FormGroup;
  dtOptions: DataTables.Settings = {};
  supplierList: Supplier[];

  errorMessages = {
    supplierName: {
      pattern: 'Invalid Format. Only - . , # / \\ \" \' ( ) special characters are allowed.',
      required: 'Supplier Name is required.',
    },
    supplierContact: {
      pattern: 'Invalid cellphone number format. Ex: XXXX-XXX-XXXX',
      required: 'Supplier Contact Number is required.',
    },
    supplierAddress: {
      pattern: 'Invalid Format. Only - . , # / \\ \" \' ( ) special characters are allowed.',
      required: 'Supplier Address is required.',
    },
  }

  supplier: Supplier = {
    id: 0,
    supplierName: '',
    address: '',
    contactNumber: ''
  };

  dtTrigger: Subject<Supplier> = new Subject();

  constructor(private title: Title,
              private formBuilder: FormBuilder,
              private supplierService: SupplierService) {
    title.setTitle("Supplier - In N Out");
  }

  ngOnInit() {
    $('#admin-nav li').removeClass('active');
    $('#admin-nav li#supplier').addClass('active');

    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10
    };
    this.getAllSuppliers();
    this.buildForm();
    $('#editSupplierModal').modal();
    $('#addSupplierModal').modal();
  }

  buildForm() {
    this.editSupplierForm = this.formBuilder.group({
      supplierName: ['', Validators.compose([
        Validators.required,
        Validators.pattern(
          '([\\w*-*.*,*#*\\/*"*\\*\'*(*)* *])*'),
      ]),
      ],
      supplierContact: ['', Validators.compose([
        Validators.required,
        Validators.pattern(
          '^([0-9]( |-)?)?(\\(?[0-9]{4}\\)?|[0-9]{3})( |-)?([0-9]{3}( |-)?[0-9]{4}|[a-zA-Z0-9]{7})'),
      ]),
      ],
      supplierAddress: ['', Validators.compose([
        Validators.required,
        Validators.pattern(
          '([\\w*-*.*,*#*\\/*"*\\*\'*(*)* *])*'),
      ]),
      ],
    });
      this.addSupplierForm = this.formBuilder.group({
        addsupplierName: ['', Validators.compose([
          Validators.required,
          Validators.pattern(
            '([\\w*-*.*,*#*\\/*"*\\*\'*(*)* *])*'),
        ]),
        ],
        addsupplierContact: ['', Validators.compose([
          Validators.required,
          Validators.pattern(
            '^([0-9]( |-)?)?(\\(?[0-9]{4}\\)?|[0-9]{3})( |-)?([0-9]{3}( |-)?[0-9]{4}|[a-zA-Z0-9]{7})'),
        ]),
        ],
        addsupplierAddress: ['', Validators.compose([
          Validators.required,
          Validators.pattern(
            '([\\w*-*.*,*#*\\/*"*\\*\'*(*)* *])*'),
        ]),
        ],
      });
  }

  resetForm() {
    this.addSupplierForm.reset();
    this.editSupplierForm.reset();
  }

  getAllSuppliers() {
    this.supplierService.getAllSuppliers().subscribe(result => {
      this.supplierList = result;
      this.dtTrigger.next();
    });
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }

  openEditModal(supplier: Supplier) {
    this.supplier.id = supplier.id;
    this.editSupplierForm.controls.supplierName.setValue(supplier.supplierName);
    this.editSupplierForm.controls.supplierAddress.setValue(supplier.address);
    this.editSupplierForm.controls.supplierContact.setValue(supplier.contactNumber);
    $('#editSupplierModal').modal('open');
  }

  openAddModal() {
    $('#addSupplierModal').modal('open');
  }

  addSupplier() {
    this.supplier.supplierName = this.addSupplierForm.controls.addsupplierName.value;
    this.supplier.address = this.addSupplierForm.controls.addsupplierAddress.value;
    this.supplier.contactNumber = this.addSupplierForm.controls.addsupplierContact.value;

    this.supplierService.store(this.supplier).subscribe(() => {
      //@ts-ignore
      swal({
        title: "Add Supplier Success!",
        text: "New Supplier has been added successfully!",
        icon: "success",
        buttons: false,
        timer: 2000,
      }).then(() => {
        this.dtTrigger.unsubscribe();
        this.getAllSuppliers();
        $('#addSupplierModal').modal('close');
      });
      $('#addSupplierModal').modal('close');
    }, (error: any) => {
      console.log(error);
    });
  }


  editSupplier() {
    this.supplier.supplierName = this.editSupplierForm.controls.supplierName.value;
    this.supplier.address = this.editSupplierForm.controls.supplierAddress.value;
    this.supplier.contactNumber = this.editSupplierForm.controls.supplierContact.value;

    this.supplierService.edit(this.supplier).subscribe((response) => {
      //@ts-ignore
      swal({
        title: "Update Success!",
        text: "Supplier information has been updated.",
        icon: "success",
        buttons: false,
        timer: 2000,
      }).then(() => {
        this.rerender();
        $('#editSupplierModal').modal('close');
      });
    }, (error: any) => {
      console.log(error);
    });
  }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      this.getAllSuppliers();
    });
  }

  delete(supplier: Supplier) {
    //@ts-ignore
    swal({
      title: "Delete Supplier?",
      text: "Are you sure you want to delete " + supplier.supplierName + "?",
      icon: "warning",
      buttons: true,
    }).then(yes => {
      if (yes) {
        this.supplierService.delete(supplier.id).subscribe(() => {
          this.rerender();
          //@ts-ignore
          swal({
            title: "Delete Success!",
            text: "Supplier information has been deleted.",
            icon: "success",
            buttons: false,
            timer: 2000,
          });
        }, (error: any) => {
          console.log(error);
        });
      }
    });


  }
}
