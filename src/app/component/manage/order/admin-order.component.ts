import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {OrderService} from "../../../service/order/order.service";
import * as moment from "moment";
import {Invoice} from "../../../model/invoice";
import {InvoiceService} from "../../../service/invoice/invoice.service";
import swal from "sweetalert";
import {Subject} from "rxjs/internal/Subject";
import {Inventory} from "../../../model/inventory";
import {DataTableDirective} from "angular-datatables";
import {OrderItemList} from "../../../model/order-item-list";
import {forEach} from "@angular/router/src/utils/collection";

@Component({
  selector: 'app-admin-order',
  templateUrl: './admin-order.component.html',
  styleUrls: ['./admin-order.component.css']
})
export class AdminOrderComponent implements OnInit, OnDestroy {

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

  invoiceList: Invoice[];
  orderItem: OrderItemList[];
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<Inventory> = new Subject();


  subtotal = 0;
  total = 0;

  constructor(private invoiceService: InvoiceService,
              private orderService: OrderService) {
  }

  ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 5
    };
    $('#viewOrderModal').modal();
    this.getAllPending();
  }

  viewOrderItems(invoice: Invoice) {
    this.orderItem = invoice.order.orderItemList;
    this.computeTotal();
    $('#viewOrderModal').modal('open');
  }

  computeSubTotal(price: number, quantity: number) {
    this.subtotal = price * quantity;
    return this.subtotal;
  }
  computeTotal() {
    this.total = 0;
    this.orderItem.forEach((item) => {
      this.total += item.quantity * item.product.price;
    });
  }

  getAllPending() {
    this.invoiceService.getAllPending().subscribe(result => {
      this.invoiceList = result;
      console.log(this.invoiceList);
      this.dtTrigger.next();
    });
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      this.getAllPending();
    });
  }

  acceptOrder(invoice: Invoice) {
    // @ts-ignore
    swal({
      title: "Are you sure you want to accept order?",
      icon: "warning",
      buttons: true,
    }).then((accept) => {
      if (accept) {
        let ctr = 0;
        this.invoiceList.forEach(item => {
          if (item.order.id === invoice.order.id) {
            invoice.order.status = "PAID";
            this.orderService.update(invoice).subscribe();
            this.rerender();
          }
          ctr++;
        })

      }
    });
  }
}
