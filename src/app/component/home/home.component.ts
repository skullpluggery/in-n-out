import {AfterViewInit, Component, OnInit} from '@angular/core';
import {Title} from "@angular/platform-browser";
import shave from 'shave';
import { fadeInAnimation } from '../../_animation';
import {Inventory} from "../../model/inventory";
import {InventoryService} from "../../service/inventory/inventory.service";
import swal from "sweetalert";
import {Product} from "../../model/product";
import {ProductService} from "../../service/product/product.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  animations: [fadeInAnimation],
  host: { '[@fadeInAnimation]': '' }
})
export class HomeComponent implements OnInit,AfterViewInit {

  constructor(private title: Title, private inventoryService: InventoryService,
              private productService: ProductService) {
    title.setTitle("Home - In N Out");
  }

  greetings = 'GOOD DAY, MATE';
  tagLine = 'Start finding the pleasure of variety on your plate on the go.';
  latestInventory: Inventory[];
  topProducts: Product[]
  user  = JSON.parse(localStorage['currentUser'] || null);

  ngOnInit(): void {
    $("#homeNav").show();
    this.getLatestProduct();
    this.getTopProducts();
  }

  getLatestProduct(){
    this.inventoryService.getLatestProducts().subscribe(result => {
      this.latestInventory = result;
    });
    setTimeout(() =>
    {
      shave('.food-desc', 120);
    }, 500);
  }

  getTopProducts(){
    this.productService.getTopProducts().subscribe(result => {
      this.topProducts = result;
    });
    setTimeout(() =>
    {
      shave('.food-desc', 120);
    }, 500);
  }


  ngAfterViewInit(): void {
    $('nav').removeClass('scrolled');
    $('nav').removeClass('z-depth-1');
  }

  //TODO PAO
  classifyUser() {
    if(this.user!=null && this.user.type!=0 && this.user.activeLastMonth){
      if(this.user.classification==="At risk") {
        swal("You must be really hungry. Don't worry, we got you ;)",
          "Redeem points through code: INNOUT100 and start ordering now!");
      }
      else if(this.user.classification==="Normal"){
        swal("Hi there! Craving for some food?", "Refer a friend and earn load points!");
      }
      else{
        swal("Thank you for your loyalty.",
          "As a reward, redeem points through code: INNOUT50!");
      }
    }
  }
}
