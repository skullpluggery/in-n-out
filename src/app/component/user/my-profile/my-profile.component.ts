import { Component, OnInit } from '@angular/core';
import {Title} from "@angular/platform-browser";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {UserService} from "../../../service/user/user.service";
import {Router} from "@angular/router";
import {User} from "../../../model/user";
import {LoyaltyCard} from "../../../model/loyalty-card";
import * as moment from "moment";
import swal from "sweetalert";
import {first} from "rxjs/operators";

@Component({
  selector: 'app-my-profile',
  templateUrl: './my-profile.component.html',
  styleUrls: ['./my-profile.component.css']
})
export class MyProfileComponent implements OnInit {

  constructor(
    private title: Title,
    private formBuilder: FormBuilder,
    private  userService: UserService,
    private router: Router) {
    title.setTitle('My Profile - In N Out');
  }

  userForm: FormGroup;

  user: User = JSON.parse(localStorage['currentUser'] || null);
  loading = false;

  errorMessages = {
    username: {
      minlength: 'Username must be at least 5 characters long.',
      required: 'Username is required.',
    },
    name: {
      minlength: 'Name must be at least 5 characters long.',
      maxlength: 'Name name cannot be more than 100 characters long.',
      required: 'Name is required.',
    },
    birthdate: {
      required: 'Birthday is required.',
    },
    email: {
      pattern: 'Invalid email format. Ex: example@example.com',
      required: 'E-mail is required.',
    },
    contactNumber: {
      pattern: 'Invalid cellphone number format. Ex: XXXX-XXX-XXXX',
      required: 'Contact Number is required.',
    },
    address: {
      minlength: 'Address must be at least 10 characters long.',
      maxlength: 'Address cannot be more than 255 characters long.',
      required: 'Address is required.',
    },
    password: {
      required: 'Password is required.',
      minlength: 'Password must be at least 8 characters long.'
    },
    confirmPassword: {
      notEquivalent: 'The inputted password is not the same.',
      required: 'Please confirm your password.',
    }
  }


  ngOnInit() {
    $('nav').removeClass('scrolled');
    $('nav').removeClass('z-depth-1');
    this.buildForm();
    this.setFormValues();
  }

  setFormValues(){
    this.userService.getById(this.user.id).subscribe(result=>{
      localStorage.setItem('currentUser', JSON.stringify(result));
      this.userForm.controls.address.setValue(result.address);
      this.userForm.controls.birthdate.setValue(moment().format(result.birthdate.toString()));
      this.userForm.controls.contactNumber.setValue(result.contactNumber);
      this.userForm.controls.email.setValue(result.email);
      this.userForm.controls.gender.setValue(result.gender);
      this.userForm.controls.name.setValue(result.name);
      this.userForm.controls.password.setValue(result.password);
      this.userForm.controls.username.setValue(result.username);
      this.userForm.disable();
      this.user.id= result.id;
      this.user.loyaltyCard = result.loyaltyCard;
      this.user.birthdate = result.birthdate;
      console.log(this.user);
    });
  }

  editForm(){
    this.userForm.enable();
  }


  maxDate = moment().subtract(10, 'years').calendar();
  public options: Pickadate.DateOptions = {
    selectMonths: true,
    selectYears: 100,
    format: 'dd-mmm-yy',
    formatSubmit: 'dd-mmm-yy',
    max: new Date(this.maxDate)
  };

  buildForm() {

    this.userForm = this.formBuilder.group({
      username: [this.user.username, Validators.compose([
        Validators.required,
        Validators.minLength(5),
      ]),
      ],
      name: [this.user.name, Validators.compose([
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(100),
      ]),
      ],
      birthdate: [this.user.birthdate, Validators.compose([
        Validators.required,
      ]),
      ],
      gender: [this.user.gender],
      email: [this.user.email, Validators.compose([
        Validators.required,
        Validators.pattern(
          '^([a-zA-Z0-9]+(?:[.-]?[a-zA-Z0-9]+)*@[a-zA-Z0-9]+(?:[.-]?[a-zA-Z0-9]+)*\\.[a-zA-Z]{2,7})$'),
      ]),
      ],
      contactNumber: [this.user.contactNumber, Validators.compose([
        Validators.required,
        Validators.pattern(
          '^([0-9]( |-)?)?(\\(?[0-9]{4}\\)?|[0-9]{3})( |-)?([0-9]{3}( |-)?[0-9]{4}|[a-zA-Z0-9]{7})'),
      ]),
      ],
      address: [this.user.address, Validators.compose([
        Validators.required,
        Validators.minLength(10),
        Validators.maxLength(255),
      ]),
      ],
      password: [this.user.password, Validators.compose([
        Validators.required,
        Validators.minLength(8),
      ]),
      ],
      confirmPassword: ['', Validators.compose([
        Validators.required,
      ]),
      ],
    }, {
      validator: this.checkIfMatchingPasswords(
        'password', 'confirmPassword')
    });
  }

  checkIfMatchingPasswords(passwordKey: string, passwordConfirmationKey: string) {
    return (group: FormGroup) => {
      let passwordInput = group.controls[passwordKey],
        passwordConfirmationInput = group.controls[passwordConfirmationKey];
      if (passwordInput.value !== passwordConfirmationInput.value) {
        return passwordConfirmationInput.setErrors({notEquivalent: true})
      }
      else {
        return passwordConfirmationInput.setErrors(null);
      }
    }
  }

  clearForm() {
    this.userForm.reset();
  }

  onSubmit() {
    this.loading = true;
    this.userForm.disable();

    this.user.name = this.userForm.controls.name.value.trim();
    this.user.username = this.userForm.controls.username.value.trim();
    this.user.birthdate = moment().format(this.userForm.controls.birthdate.value);
    this.user.gender = this.userForm.controls.gender.value;
    this.user.email = this.userForm.controls.email.value.trim();
    this.user.contactNumber = this.userForm.controls.contactNumber.value.trim();
    this.user.address = this.userForm.controls.address.value.trim();
    this.user.password = this.userForm.controls.password.value.trim();
    console.log(this.user);
      this.userService.edit(this.user).subscribe((response) => {
        this.userForm.enable();
        this.loading = false;
        this.clearForm();
        //@ts-ignore
        swal({
          title: "Edit Profile Success!",
          text: "You will be redirected in a few seconds...",
          icon: "success",
          buttons: false,
          timer: 2000,
        }).then(() => {
          this.userService.signIn(response.username.toString(), response.password.toString()).pipe(first())
            .subscribe((user) => {
              location.reload();
            });
        });
      }, (error: any) => {
        this.userForm.enable();
        this.loading = false;
        console.log(error);
      });

  }
}
