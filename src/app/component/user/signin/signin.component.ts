import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Title} from "@angular/platform-browser";
import {UserService} from "../../../service/user/user.service";
import {first} from "rxjs/operators";
import swal from "sweetalert";
import {Router} from "@angular/router";


@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SignInComponent implements OnInit {

  constructor(private title: Title,
              private formBuilder: FormBuilder,
              private userService: UserService,
              private router: Router) {}

  userForm: FormGroup;

  errorMessageResources = {
    username: {
      required: 'Username or E-mail is required.',
    },
    password: {
      required: 'Password is required.',
    }
  }

  ngOnInit() {
    this.buildForm();
  }

  buildForm() {
    this.userForm = this.formBuilder.group({
      username: ['', Validators.compose([
        Validators.required,
      ]),
      ],
      password: ['', Validators.compose([
        Validators.required,
      ]),
      ],
    })
  }

  resetForm() {
    this.userForm.reset();
  }

  onSubmit() {
    if (!this.userForm.valid) {
      return;
    }

    this.userService.signIn(this.userForm.controls.username.value,
      this.userForm.controls.password.value)
      .pipe(first()).subscribe(
        (user) => {
          if(user){
            //@ts-ignore
            swal({
              title: "Login Success!",
              text: "You will be redirected in a few seconds...",
              icon: "success",
              buttons: false,
              timer: 2000,
            }).then(()=>{
              if(user.type==0){
                this.router.navigate(['/admin/manage']);
                setTimeout(() =>location.reload(), 100);

              }else{
                this.router.navigate(['/']);
                location.reload();
              }
            });
          }else{
            //@ts-ignore
            swal({
              title: "Login Failed!",
              text: "Please double check your username or password.",
              icon: "error",
              timer: 2000,
            })
          }
        },
        error => {
          console.log(error);
        });
  }
}
