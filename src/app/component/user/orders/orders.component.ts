import {Component, OnDestroy, OnInit} from '@angular/core';
import {Order} from "../../../model/order";
import {Invoice} from "../../../model/invoice";
import {Subject} from "rxjs/internal/Subject";
import {Inventory} from "../../../model/inventory";
import {InvoiceService} from "../../../service/invoice/invoice.service";
import {OrderService} from "../../../service/order/order.service";
import swal from "sweetalert";
import {User} from "../../../model/user";
import {OrderItemList} from "../../../model/order-item-list";

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit, OnDestroy {

  invoiceList : Invoice[];
  orderItem: OrderItemList[];
  user: User  = JSON.parse(localStorage['currentUser'] || null);
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<Inventory> = new Subject();

  subtotal = 0;
  total = 0;
  constructor(private invoiceService: InvoiceService,
              private orderService: OrderService) {
  }

  ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 5
    };
    $('#viewOrderModal').modal();
    this.getAllInvoiceByUser();
  }

  viewOrderItems(invoice: Invoice){
    this.orderItem = invoice.order.orderItemList;
    this.computeTotal();
    $('#viewOrderModal').modal('open');
  }

  computeSubTotal(price:number, quantity:number) {
    this.subtotal = price * quantity;
    return this.subtotal;
  }

  computeTotal() {
    this.total = 0;
    this.orderItem.forEach((item) => {
      this.total += item.quantity * item.product.price;
    });
  }

  getAllInvoiceByUser() {
    this.invoiceService.getAllInvoiceByUser(this.user.id).subscribe(result => {
      this.invoiceList = result;
      this.dtTrigger.next();
    });
  }

  ngOnDestroy(): void {
    this.dtTrigger.unsubscribe();
  }

}
