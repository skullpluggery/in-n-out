import {Component, OnInit} from '@angular/core';
import {Title} from "@angular/platform-browser";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {User} from "../../../model/user";
import {UserService} from "../../../service/user/user.service";
import {fadeInAnimation} from "../../../_animation";
import {Router} from "@angular/router";
import swal from "sweetalert";
import {LoyaltyCard} from "../../../model/loyalty-card";
import {first} from "rxjs/operators";
import * as moment from 'moment';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css'],
  animations: [fadeInAnimation],
  host: {'[@fadeInAnimation]': ''}
})
export class SignUpComponent implements OnInit {

  constructor(
    private title: Title,
    private formBuilder: FormBuilder,
    private  userService: UserService,
    private router: Router) {
    title.setTitle('Sign Up - In N Out');
  }

  userForm: FormGroup;
  user: User = {
    id: 0,
    name: '',
    username: '',
    email: '',
    contactNumber: '',
    address: '',
    type: 1,
    password: '',
    birthdate: '',
    gender: 'M',
    loyaltyCard: new LoyaltyCard(),
    classification: '',
    activeLastMonth: false
  }
  loading = false;

  errorMessages = {
    username: {
      minlength: 'Username must be at least 5 characters long.',
      required: 'Username is required.',
    },
    name: {
      minlength: 'Name must be at least 5 characters long.',
      maxlength: 'Name name cannot be more than 100 characters long.',
      required: 'Name is required.',
    },
    birthdate: {
      required: 'Birthday is required.',
    },
    email: {
      pattern: 'Invalid email format. Ex: example@example.com',
      required: 'E-mail is required.',
    },
    contactNumber: {
      pattern: 'Invalid cellphone number format. Ex: XXXX-XXX-XXXX',
      required: 'Contact Number is required.',
    },
    address: {
      minlength: 'Address must be at least 10 characters long.',
      maxlength: 'Address cannot be more than 255 characters long.',
      required: 'Address is required.',
    },
    password: {
      required: 'Password is required.',
      minlength: 'Password must be at least 8 characters long.'
    },
    confirmPassword: {
      notEquivalent: 'The inputted password is not the same.',
      required: 'Please confirm your password.',
    }
  }


  ngOnInit() {
    $('nav').removeClass('scrolled');
    $('nav').removeClass('z-depth-1');
    this.buildForm();
    if(JSON.parse(localStorage['currentUser'] || null)){
      this.router.navigate(['/']);
    }

    console.log(this.user);
  }

  maxDate = moment().subtract(10, 'years').calendar();
  public options: Pickadate.DateOptions = {
    selectMonths: true,
    selectYears: 100,
    format: 'dd-mmm-yy',
    formatSubmit: 'yyyy-mm-dd',
    max: new Date(this.maxDate)
  };


  buildForm() {
    this.userForm = this.formBuilder.group({
      username: [this.user.username, Validators.compose([
        Validators.required,
        Validators.minLength(5),
      ]),
      ],
      name: [this.user.name, Validators.compose([
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(100),
      ]),
      ],
      birthdate: [this.user.birthdate, Validators.compose([
        Validators.required,
      ]),
      ],
      gender: [this.user.gender],
      email: [this.user.email, Validators.compose([
        Validators.required,
        Validators.pattern(
          '^([a-zA-Z0-9]+(?:[.-]?[a-zA-Z0-9]+)*@[a-zA-Z0-9]+(?:[.-]?[a-zA-Z0-9]+)*\\.[a-zA-Z]{2,7})$'),
      ]),
      ],
      contactNumber: [this.user.contactNumber, Validators.compose([
        Validators.required,
        Validators.pattern(
          '^([0-9]( |-)?)?(\\(?[0-9]{4}\\)?|[0-9]{3})( |-)?([0-9]{3}( |-)?[0-9]{4}|[a-zA-Z0-9]{7})'),
      ]),
      ],
      address: [this.user.address, Validators.compose([
        Validators.required,
        Validators.minLength(10),
        Validators.maxLength(255),
      ]),
      ],
      password: [this.user.password, Validators.compose([
        Validators.required,
        Validators.minLength(8),
      ]),
      ],
      confirmPassword: ['', Validators.compose([
        Validators.required,
      ]),
      ],
    }, {
      validator: this.checkIfMatchingPasswords(
        'password', 'confirmPassword')
    });
  }

  checkIfMatchingPasswords(passwordKey: string, passwordConfirmationKey: string) {
    return (group: FormGroup) => {
      let passwordInput = group.controls[passwordKey],
        passwordConfirmationInput = group.controls[passwordConfirmationKey];
      if (passwordInput.value !== passwordConfirmationInput.value) {
        return passwordConfirmationInput.setErrors({notEquivalent: true})
      }
      else {
        return passwordConfirmationInput.setErrors(null);
      }
    }
  }

  clearForm() {
    this.userForm.reset();
  }

  onSubmit() {
    this.loading = true;
    this.userForm.disable();

    this.user.name = this.userForm.controls.name.value.trim();
    this.user.username = this.userForm.controls.username.value.trim();
    this.user.birthdate =  moment().format(this.userForm.controls.birthdate.value.toString());
    this.user.gender = this.userForm.controls.gender.value;
    this.user.email = this.userForm.controls.email.value.trim();
    this.user.contactNumber = this.userForm.controls.contactNumber.value.trim();
    this.user.address = this.userForm.controls.address.value.trim();
    this.user.password = this.userForm.controls.password.value.trim();

    this.userService.createLoyaltyCard().subscribe((response) => {
      console.log(response);
      this.user.loyaltyCard = response;
      this.userService.signUp(this.user).subscribe((response) => {
        this.userForm.enable();
        this.loading = false;
        this.clearForm();
        //@ts-ignore
        swal({
          title: "Sign Up Success!",
          text: "You will be redirected in a few seconds...",
          icon: "success",
          buttons: false,
          timer: 2000,
        }).then(() => {
          this.userService.signIn(response.username.toString(), response.password.toString()).pipe(first())
            .subscribe((user) => {
                location.reload();
              });
        });
      }, (error: any) => {
        this.userForm.enable();
        this.loading = false;
        console.log(error);
      });
    });

  }
}
