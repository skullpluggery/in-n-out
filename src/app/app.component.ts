import {AfterViewInit, Component, OnInit} from '@angular/core';
import {UserService} from "./service/user/user.service";
import shave from 'shave';
import {Cart} from "./model/cart";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit, AfterViewInit {

  public static API_URL = "http://localhost:8000";

  static cart: Cart = {
    cartItems: [],
    itemCount: 0,
    totalQTY: 0,
    totalPrice: 0,
  };


  user = JSON.parse(localStorage['currentUser'] || null);

  private static _cartCount = 0;

  get cartCount(): number {
    return AppComponent._cartCount;
  }

  constructor(private userService: UserService) {
    AppComponent.cart.itemCount = AppComponent.cart.cartItems.length;
    AppComponent._cartCount = AppComponent.cart.itemCount;
  }

  ngOnInit() {
    $('.modal').modal();
    $(document).scroll(function () {
      const $nav = $('nav:not(#admin-nav)');
      $nav.toggleClass('scrolled z-depth-1',
        ($(this).scrollTop() > $nav.height()));
    });

  }

  ngAfterViewInit(): void {

  }


  signOut() {
    this.userService.signOut()
  }

  static syncCart() {
    AppComponent.cart.itemCount = AppComponent.cart.cartItems.length;
    AppComponent.cart.totalPrice = 0;
    AppComponent.cart.totalQTY = 0;
    AppComponent.cart.cartItems.forEach((item) => {
      AppComponent.cart.totalPrice += item.subtotalPrice;
      AppComponent.cart.totalQTY += item.quantity;
    });
    this._cartCount = AppComponent.cart.itemCount;
  }

  viewCart() {
    setTimeout(() =>
      shave('.cart-item-desc', 50), 500);
  }


}

