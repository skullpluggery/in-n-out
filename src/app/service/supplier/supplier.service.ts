import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/internal/Observable";
import {Supplier} from "../../model/supplier";
import {AppComponent} from "../../app.component";

@Injectable({
  providedIn: 'root'
})
export class SupplierService {

  constructor(private http: HttpClient) {
  }

  store(supplier): Observable<Supplier> {
    return this.http.post<Supplier>(AppComponent.API_URL + '/supplier', supplier);
  }

  getAllSuppliers(): Observable<Supplier[]> {
    return this.http.get<Supplier[]>(AppComponent.API_URL + '/supplier');
  }

  showSupplierByName(keyword: String): Observable<Supplier> {
    return this.http.get<Supplier>(AppComponent.API_URL + '/supplier/profile/' + keyword);
  }


  show(id: number): Observable<Supplier> {
    return this.http.get<Supplier>(AppComponent.API_URL + '/supplier/' + id);
  }

  edit(supplier): Observable<Supplier> {
    return this.http.put<Supplier>(AppComponent.API_URL + '/supplier', supplier);
  }

  delete(id: number): Observable<Supplier> {
    return this.http.delete<Supplier>(AppComponent.API_URL + '/supplier/' + id);
  }


}
