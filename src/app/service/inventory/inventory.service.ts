import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/internal/Observable";
import {Inventory} from "../../model/inventory";
import {AppComponent} from "../../app.component";
import {Product} from "../../model/product";
@Injectable({
  providedIn: 'root'
})
export class InventoryService {

  constructor(private http: HttpClient) {
  }

  getAllInventory(): Observable<Inventory[]> {
    return this.http.get<Inventory[]>(AppComponent.API_URL + '/inventory');
  }

  showInventoryByNameOrCategory(keyword: String): Observable<Inventory[]> {
    return this.http.get<Inventory[]>(AppComponent.API_URL + '/inventory/profile/' + keyword);
  }

  getLatestProducts(): Observable<Inventory[]> {
    return this.http.get<Inventory[]>(AppComponent.API_URL + '/inventory/latest');
  }

  store(inventory):  Observable<Inventory> {
    return this.http.post<Inventory>(AppComponent.API_URL+'/inventory', inventory);
  }

  edit(inventory): Observable<Inventory> {
    return this.http.put<Inventory>(AppComponent.API_URL + '/inventory', inventory);
  }

  delete(id: number): Observable<Inventory> {
    return this.http.delete<Inventory>(AppComponent.API_URL + '/inventory/' + id);
  }
}
