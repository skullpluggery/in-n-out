import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/internal/Observable";
import {Product} from "../../model/product";
import {AppComponent} from "../../app.component";

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private  http: HttpClient) { }


  getTopProducts(): Observable<Product[]> {
    return this.http.get<Product[]>(AppComponent.API_URL + '/product/top');
  }

  uploadImage(image){
    let formData = new FormData;
    formData.append("image", image, image.name);
    return this.http.post(AppComponent.API_URL+'/product/img/upload', formData);

  }

  store(product):  Observable<Product> {
    return this.http.post<Product>(AppComponent.API_URL+'/product', product);
  }

  edit(product): Observable<Product> {
    return this.http.put<Product>(AppComponent.API_URL + '/product', product);
  }

  delete(id: number): Observable<Product> {
    return this.http.delete<Product>(AppComponent.API_URL + '/product/' + id);
  }

}



