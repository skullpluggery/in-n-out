import { Injectable } from '@angular/core';
import {Observable} from "rxjs/internal/Observable";
import {HttpClient} from "@angular/common/http";
import {Invoice} from "../../model/invoice";
import {AppComponent} from "../../app.component";

@Injectable({
  providedIn: 'root'
})
export class InvoiceService {

  constructor(private http: HttpClient) { }


  getAllInvoiceByUser(id :number): Observable<Invoice[]> {
    return this.http.get<Invoice[]>(AppComponent.API_URL+ '/invoice/user/'+id);  }

  getAllPending(): Observable<Invoice[]> {
    return this.http.get<Invoice[]>(AppComponent.API_URL+ '/invoice/pending');  }
}
