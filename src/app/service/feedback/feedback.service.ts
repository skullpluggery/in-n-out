import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/internal/Observable";
import {Feedback} from "../../model/feedback";
import {AppComponent} from "../../app.component";

@Injectable({
  providedIn: 'root'
})
export class FeedbackService {

  constructor(private http: HttpClient) {
  }

  store(feedback): Observable<Feedback> {
    return this.http.post<Feedback>(AppComponent.API_URL + '/feedback', feedback);
  }

  getAll():Observable<Feedback[]>{
    return this.http.get<Feedback[]>(AppComponent.API_URL+'/feedback');
  }
}
