import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {LoyaltyCard} from "../../model/loyalty-card";
import {Observable} from "rxjs/internal/Observable";
import {User} from "../../model/user";
import {AppComponent} from "../../app.component";

@Injectable({providedIn: 'root'})
export class UserService {

  constructor(private http: HttpClient) {
  }

  edit(user): Observable<User> {
    return this.http.put<User>(AppComponent.API_URL + '/user', user);
  }

  signUp(user): Observable<User> {
    return this.http.post<User>(AppComponent.API_URL + '/user', user);
  }


  createLoyaltyCard(): Observable<LoyaltyCard> {
    let loyaltyCard = new LoyaltyCard;
    loyaltyCard.cardNumber = 'XXXX-XXXX-XXXX-XXXX';
    loyaltyCard.validity = '24-AUG-2050';AppComponent
    loyaltyCard.loadPoints = 0;
    loyaltyCard.loyaltyPoints = 0;

    return this.http.post<LoyaltyCard>(AppComponent.API_URL + '/loyaltyCard', loyaltyCard);
  }

  updateLoyaltyCard(loyaltyCard: LoyaltyCard) {
    return this.http.put(AppComponent.API_URL+'/loyaltyCard', loyaltyCard);
  }

  signIn(usernameOrEmail: string, password: string) {
    let body = new FormData();
    body.append('usernameOrEmail', usernameOrEmail);
    body.append('password', password);

    return this.http.post<User>(AppComponent.API_URL+'/user/signIn', body)
      .pipe(map(user => {
        if (user) {
          localStorage.setItem('currentUser', JSON.stringify(user));
        }
        return user;
      }));
  }

  syncUser() {
    let user: User = JSON.parse(localStorage['currentUser'] || null);
    return this.http.get<User>(AppComponent.API_URL + '/user/' + user.id);
  }

  getById(id) {
    return this.http.get<User>(AppComponent.API_URL + '/user/' + id);
  }

  signOut() {
    localStorage.removeItem('currentUser');
    location.reload();
  }
}
