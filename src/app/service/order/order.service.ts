import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/internal/Observable";
import {Order} from "../../model/order";
import {Invoice} from "../../model/invoice";
import {AppComponent} from "../../app.component";

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(private http: HttpClient) { }

  store(order) {
    return this.http.post(AppComponent.API_URL+'/order', order);
  }

  getAll() : Observable<Order[]>{
    return this.http.get<Order[]>(AppComponent.API_URL+'/order');
  }

  // update(invoice) {
  //   return this.http.put<Invoice>(AppComponent.API_URL+'/order',invoice);
  // }

  update(invoice) {
    return this.http.post<Invoice>(AppComponent.API_URL+'/order/confirmOrder',invoice);
  }
}
